﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using NLog;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Timers;

namespace ITC_Tranx_RevenueCodeForNotificationNEW
{
    public partial class TranxWithReveueCode : ServiceBase
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        private void GetRecordFromEcashier()
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["eCashierDB"].ConnectionString);
            sqlConnection.Open();
            try
            {
                try
                {
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(new SqlCommand("select p.TransactionReferenceNo,pi.PaymentItemCode,pi.PaymentItemName,p.Amount,p.CustomerName,p.DepositorName from PaymentTransactions p,PaymentTransactionitems pi where exists(select* from PaymentTransactionitems where PaymentItemCode = pi.PaymentItemCode) and p.TransactionId = pi.TransactionId and cast(p.PaymentDate as Date) = cast(getdate() as Date) order by p.TransactionId desc", sqlConnection));
                    DataTable dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);
                    string empty = string.Empty;
                    for (int i = 0; dataTable.Rows.Count > i; i++)
                    {
                        empty = string.Concat(new string[] { empty, "Update PaymentTransactions set revenuecode='", dataTable.Rows[i].ItemArray.GetValue(1).ToString().Replace("'", "''"), "',CustomerName='", dataTable.Rows[i].ItemArray.GetValue(4).ToString().Replace("'", "''"), "',DepositorName='", dataTable.Rows[i].ItemArray.GetValue(5).ToString().Replace("'", "''"), "' WHERE transId='", dataTable.Rows[i].ItemArray.GetValue(0).ToString().Replace("'", "''"), "'; " });
                    }
                    string str = empty;
                    SqlConnection sqlConnection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["XpressDb"].ConnectionString);
                    sqlConnection1.Open();
                    try
                    {
                        try
                        {
                            (new SqlCommand(str, sqlConnection1)).ExecuteNonQuery();
                        }
                        catch (Exception exception)
                        {
                            string message = exception.Message;
                        }
                    }
                    finally
                    {
                        sqlConnection1.Close();
                        sqlConnection1.Dispose();
                    }
                }
                catch (Exception exception1)
                {
                    string message1 = exception1.Message;
                }
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }
        public TranxWithReveueCode()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            DateTime now = DateTime.Now;
            logger.Info(string.Concat("SERVICE STARTED AT ", now.ToString()));
            int num = Convert.ToInt32(ConfigurationManager.AppSettings["interval"]);
            Timer timer = new Timer()
            {
                Interval = (double)num
            };
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        protected override void OnStop()
        {
            DateTime now = DateTime.Now;
            logger.Info(string.Concat("SERVICE STOPPED AT ", now.ToString()));
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            try
            {
                if (!Directory.Exists("C:\\LOG-NOTIFICATION-REVENUE"))
                {
                    Directory.CreateDirectory("C:\\LOG-NOTIFICATION-REVENUE");
                }
            }
            catch (Exception exception)
            {
                string message = exception.Message;
            }
            this.GetRecordFromEcashier();
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["XpressDb"].ConnectionString);
            sqlConnection.Open();
            try
            {
                try
                {
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(new SqlCommand(" select C.MerchantCode,p.TransId,p.Amount,p.MaskedCardPan,p.TerminalId,p.DepositorName,p.CustomerName,p.RevenueCode,p.TransactionDate,p.Currency,p.ReceiptNumber,p.InstitutionCode,p.Bank,p.BankName,p.BankBranch,c.NotificationEndPoint,p.ReceiptNumber,p.Amount from PaymentTransactions p, ConsultantInfo c where exists(select * from Table_RevenueItems where RevenueCode = p.RevenueCode) and isPush IS NULL and cast(p.TransactionDate as Date) = cast(getdate() as Date) and c.ConsultantCode = 'ITC'", sqlConnection));
                    DataTable dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);
                    for (int i = 0; dataTable.Rows.Count > i; i++)
                    {
                        SqlConnection sqlConnection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["XpressDb"].ConnectionString);
                        sqlConnection1.Open();
                        try
                        {
                            try
                            {
                                string str = string.Concat(new string[] { "INSERT INTO NotificationTable(MerchantCode,TransactionId,Amount,MaskedPan,TerminalId,DepositorName,CustomerName,RevenueCode,TransactionDate,CurrencyCode,ReceiptNumber,InstitutionCode,BankCode,BankName,BranchCode,NotificationEndpoint,SubItemTransactionId,TotalAmount,ConsultanCode,Status) VALUES ('", dataTable.Rows[i].ItemArray.GetValue(0).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(1).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(2).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(3).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(4).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(5).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(6).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(7).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(8).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(9).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(10).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(11).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(12).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(13).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(14).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(15).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(16).ToString().Replace("'", "''"), "','", dataTable.Rows[i].ItemArray.GetValue(17).ToString().Replace("'", "''"), "','ITC','0'); update paymentTransactions set ispush='1' where transId='", dataTable.Rows[i].ItemArray.GetValue(1).ToString().Replace("'", "''"), "'" });
                                (new SqlCommand(str, sqlConnection1)).ExecuteNonQuery();
                                string str1 = "SUCCESSFULLY PROFILED FOR NOTIFICATION";
                                logger.Info(string.Concat(new string[] { "\n\n RESPONSE MESSAGE FOR ITC PICKED TRANSACTION WITH TRANSACTION ID ", dataTable.Rows[i].ItemArray.GetValue(1).ToString(), " ==> ", str1, "\n" }));
                            }
                            catch (Exception exception2)
                            {
                                Exception exception1 = exception2;
                                string message1 = exception1.Message;
                               logger.Error(string.Concat(new string[] { "\n\n RESPONSE MESSAGE FOR ITC PICKED TRANSACTION WITH TRANSACTION ID ", dataTable.Rows[i].ItemArray.GetValue(1).ToString(), " ==> ", exception1.Message, "\n" }));
                            }
                        }
                        finally
                        {
                            sqlConnection1.Close();
                            sqlConnection1.Dispose();
                        }
                    }
                }
                catch (Exception exception3)
                {
                }
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
        }

    }
}
